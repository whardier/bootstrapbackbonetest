# Bootstrap + Backbone.js Test

## Install.. sort of

```
virtualenv --system-site-packages ~/VirtualEnv/BackboneBootstrapTest/

source /home/sspencer/VirtualEnv/BackboneBootstrapTest/bin/activate

pip install -r requirements.txt

python ./server.py --config=server.conf
```

## Run

```
python ./server.py --config=server.conf

```

## What this shows off

- Bootstrap and Backbone.js behaving together

- Tornado serving read/write models to Backbone.js

## Have fun in the Javascript Console

```javascript
// Change the title to whatever Tornado wants to hand out.  The header has a hook to rerender whenever testModel changes.

testModel.fetch();

// Play with a local version of a testmodel

testModelLocal.attributes;

testModelLocal.get('name');

testModelLocal.get('somethingthatdoesntexist');

testModellocal.set({'name': 'I am the very model of a modern major general!'});

testModelLocal.attributes;

testModelLocal.save();

// Open up your resources and check your local storage now.

// It auto assigns potentially unique IDs :)

var id = testModelLocal.id

id;

// Lets make a clone to show how we can later retrieve this information if we know the id

var testModelLocalOther = var TestModelLocal({id: id});

testModelLocalOther.attributes;

testModelLocalOther.fetch();

testModelLocalOther.attributes; // TADA!

// Now lets send it to the server.. which we could be doing by default.. but we want extra cache control

var toSyncTestModel = new TestModel(testModelLocalOther.attributes);

toSyncTestModel.save() // check the server console
```

