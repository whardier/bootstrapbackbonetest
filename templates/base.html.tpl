<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="{% module poomodule('ico/favicon.png') %}">

    <title>Jumbotron Template for Bootstrap</title>

    <!-- Bootstrap CSS -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/css/bootstrap-theme.min.css" rel="stylesheet">

    <!-- FontAwesome CSS -->
    <link href="//cdnjs.cloudflare.com/ajax/libs/font-awesome/4.0.3/css/font-awesome.min.css" rel="stylesheet">

    <style>
        /* Move down content because we have a fixed navbar that is 50px tall */
        body {
            padding-top: 50px;
            padding-bottom: 20px;
        }
    </style>

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/r29/html5.min.js"></script>
      <script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>

<body>

    <div id="header"></div>

    <script type="text/html" id="template-header">
        <div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
            <div class="container">
                <div class="navbar-header">
                    <a class="navbar-brand" href="#"><%= name %></a>
                </div>
            </div>
        </div>
    </script>

    <div id="content"></div>

    <script type="text/html" id="template-home">
        <div class="jumbotron">
            <div class="container">
                <h1>Hello, world!</h1>
                <p>This is a template for a simple marketing or informational website. It includes a large callout called a jumbotron and three supporting pieces of content. Use it as a starting point to create something more unique.</p>
                <p><a class="btn btn-primary btn-lg" role="button">Learn more &raquo;</a>
                </p>
            </div>
        </div>
                
        <div class="container">
            <div class="row">
                <div class="col-md-4">
                    <h2>Heading</h2>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a>
                    </p>
                </div>
                <div class="col-md-4">  
                    <h2>Heading</h2>
                    <p>Donec id elit non mi porta gravida at eget metus. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet risus. Etiam porta sem malesuada magna mollis euismod. Donec sed odio dui.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a>
                    </p>
                </div>
                <div class="col-md-4">
                    <h2>Heading</h2>
                    <p>Donec sed odio dui. Cras justo odio, dapibus ac facilisis in, egestas eget quam. Vestibulum id ligula porta felis euismod semper. Fusce dapibus, tellus ac cursus commodo, tortor mauris condimentum nibh, ut fermentum massa justo sit amet
                        risus.</p>
                    <p><a class="btn btn-default" href="#" role="button">View details &raquo;</a>
                    </p>
                </div>
            </div>
            
            <hr>
            
            <footer>
                <p>&copy; Company 2013</p>
            </footer>
        </div>    
    </script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/2.0.3/jquery.min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/backbone-localstorage.js/1.1.0/backbone.localStorage-min.js"></script>

    <script src="//cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.0.3/js/bootstrap.js"></script>

    <script>

        var TestModelLocal = Backbone.Model.extend({
            localStorage: new Backbone.LocalStorage("todos"),
            defaults: {
                'name': 'Helllooooooo',
            },
        });

        var TestModel = Backbone.Model.extend({
            url: 'testModel',
            defaults: {
                'name': 'Helllooooooo',
            },
        });

        var testModel = new TestModel();
        var testModelLocal = new TestModelLocal();

    </script>

    <script>

        var HeaderView = Backbone.View.extend({
        
            initialize: function () {
                console.log('Initializing Header View');
                this.template = _.template($("#template-header").html());
                this.model.bind('change', this.render, this);
            },
        
            render: function () {
                $(this.el).html(this.template(this.model.toJSON()));
                return this;
            },
                
        });

    </script>

    <script>

        var HomeView = Backbone.View.extend({
        
            initialize:function () {
                console.log('Initializing Home View');
                this.template = _.template($("#template-home").html());
            },
        
            render:function () {
                $(this.el).html(this.template());
                return this;
            },

        });

    </script>

    <script>

        var theheader;

        var Router = Backbone.Router.extend({
        
            routes: {
                "": "home",
                "post/:id": "home2",
            },
        
            initialize: function () {
                this.headerView = new HeaderView({'model': testModel});
                theheader = this.headerView;
                $("#header").html(this.headerView.render().el);
            },
        
            home: function () {
                this.homeView = new HomeView();
                $("#content").html(this.homeView.render().el);
            },

            home2: function () {
                this.homeView = new HomeView();
                $("#content").html(this.homeView.render().el);
            },
                
        });

        $(function () {
            app = new Router();
            Backbone.history.start();
        });

    </script>

</body>

</html>

