class TornadoDataURIManager(object):

    def __init__(self, handler):
        self.handler = handler
        self.__setup_encookie()

    def __setup_encookie(self):
        pass

    def static_data_uri(self, name, value=None, max_age_days=31):
        return 'HI'

class TornadoDataURIMixin(object):
    @property
    def encookie(self):
        '''
        Returns an tornado-data-uri instance
        '''

        return create_mixin(self, '__tornado_data_uri_manager', TornadoDataURIManager)

class ConfigurationError(Exception):
    pass

def create_mixin(context, manager_property, manager_class):
    if not hasattr(context, manager_property):
        setattr(context, manager_property, manager_class(context))
    return getattr(context, manager_property)
